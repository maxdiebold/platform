// lib/get-slides

import fs from "fs"
import path from "path"

export const getSlides = (slideDir) => {
    const serverDir = path.join(process.cwd(), 'public', 'images', slideDir)
    const fileNames = fs.readdirSync(serverDir)
    const allSlides = fileNames.map((fileName) => {
        return path.join('/images', slideDir, fileName)
    })
    return allSlides
}
