// lib/events.js

import fs from "fs"
import html from "remark-rehype"
import markdown from "remark-parse"
import matter from "gray-matter"
import path from "path"
import sanitize from "rehype-sanitize"
import stringify from "rehype-stringify"
import unified from "unified"

const eventsDirectory = path.join(process.cwd(), "data", "events")

export const getSortedEventsData = () => {
    // Get file names under /events
    const fileNames = fs.readdirSync(eventsDirectory)
    const allEventsData = fileNames.map((fileName) => {
        // Remove ".md" from file name to get id
        const id = fileName.replace(/\.md$/, "")

        // Read markdown file as string
        const fullPath = path.join(eventsDirectory, fileName)
        const fileContents = fs.readFileSync(fullPath, "utf8")

        // Use gray-matter to parse the event metadata section
        const matterResult = matter(fileContents)

        // Combine the data with the id
        return {
            id,
            ...matterResult.data,
        }
    })

    // Filter out past events
    const now = new Date()
    const year = now.getFullYear()
    let month = now.getMonth() + 1
    if (month.toString().length < 2) {
        month = '0' + month
    }
    let day = now.getDate() - 1
    if (day.toString().length < 2) {
        day = '0' + day
    }
    const yesterday = `${year}-${month}-${day}`
    const currentEvents = allEventsData.filter((event) => {
        if (event.endDate) {
            return event.endDate >= yesterday
        }
        return event.date >= yesterday
    })

    // Sort events by date
    return currentEvents.sort((a, b) => {
        if (a.date > b.date) {
            return 1
        } else {
            return -1
        }
    })
}

export const getAllEventIds = () => {
    const fileNames = fs.readdirSync(eventsDirectory)

    // Returns an array that looks like this:
    // [
    //   {
    //     params: {
    //       id: 'ssg-ssr'
    //     }
    //   },
    //   {
    //     params: {
    //       id: 'pre-rendering'
    //     }
    //   }
    // ]
    return fileNames.map((fileName) => {
        return {
            params: {
                id: fileName.replace(/\.md$/, ""),
            },
        }
    })
}

export const getEventData = async (id) => {
    const fullPath = path.join(eventsDirectory, `${id}.md`)
    const fileContents = fs.readFileSync(fullPath, "utf8")

    // Use gray-matter to parse the event metadata section
    const matterResult = matter(fileContents)

    // Use remark to convert markdown into HTML string
    const processor = unified()
        .use(markdown)
        .use(html)
        .use(stringify)
        .use(sanitize)

    const processedContent = await processor.process(matterResult.content)
    const contentHtml = processedContent.contents

    // Combine the data with the id
    return {
        id,
        contentHtml,
        ...matterResult.data,
    }
}
