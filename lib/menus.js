// lib/menus.js

import fs from "fs"
import path from "path"

const menusDirectory = path.join(process.cwd(), "data", "menus")

export const getMenusData = () => {
    /* Extract JSON objects from menu files,
     * and combine into array, sorted by filename */

    const fileNames = fs.readdirSync(menusDirectory)
    const allMenusData = {}
    for (let fileName of fileNames) {
        const id = fileName.replace(/\.json$/, "")
        const fullPath = path.join(menusDirectory, fileName)
        let fileContents = fs.readFileSync(fullPath, "utf8")
        const menuData = JSON.parse(fileContents)
        allMenusData[id] = menuData
    }
    return allMenusData
}

export const getAllMenuIds = () => {
    const fileNames = fs.readdirSync(menusDirectory)

    return fileNames.map((fileName) => {
        return {
            params: {
                id: fileName.replace(/\.json$/, ""),
            },
        }
    })
}

export const getMenuData = async (id) => {
    const fullPath = path.join(menusDirectory, `${id}.json`)
    const fileContents = fs.readFileSync(fullPath)
    const menu = JSON.parse(fileContents)

    return {
        id,
        ...menu,
    }
}
