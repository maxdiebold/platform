// lib/api-helpers.ts


export async function fetchGetJSON(url: string) {
    try {
        const data = await fetch(url).then((res) => res.json())
        return data }
    catch (err) {
        throw new Error(err.message) }}

export async function fetchPostJSON(url: string, data?: {}) {
    try {
        const response = await fetch(url, {
            method: 'POST',    
            mode: 'cors',      
            cache: 'no-cache', 
            credentials: 'same-origin', 
            headers: {
                'Content-Type': 'application/json' },
            redirect: 'follow', 
            referrerPolicy: 'no-referrer',
            body: JSON.stringify(data || {}) }) 
        return await response.json() }
    catch (err) {
        throw new Error(err.message) }}

    // Default options are marked with *
    // *GET, POST, PUT, DELETE, etc.
    // no-cors, *cors, same-origin
    // *default, no-cache, reload, force-cache, only-if-cached
    // include, *same-origin, omit
    // 'Content-Type': 'application/x-www-form-urlencoded',
    // manual, *follow, error 
    // no-referrer, *client
    // body data type must match "Content-Type" header
    // parses JSON response into native JavaScript objects
