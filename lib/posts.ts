// lib/posts

import fs from 'fs'
import html from 'remark-rehype'
import markdown from 'remark-parse'
import matter from 'gray-matter'
import path from 'path'
import sanitize from 'rehype-sanitize'
import stringify from 'rehype-stringify'
import unified from 'unified'


export type PostData = {
    content: string,
    date: string,
    id: string,
    image?: string,
    title?: string }

const postsDirectory = path.join(process.cwd(), 'data', 'posts')


export const getAllPostIds = () => {
    const fileNames = fs.readdirSync(postsDirectory)

    return fileNames.map(fileName => {
        return fileName.replace(/\.md$/, '')})}

export const getAllPosts = async () => {
    const postIds = getAllPostIds()
    const allPosts = await Promise.all(
        postIds.map((id) => {
            return getPostData(id)
        })
    )
    allPosts.sort((a, b) => {
        if (a.date < b.date) {
            return 1
        } else {
            return -1
        }
    })
    return allPosts
}

export const getPostData = async (id): Promise<PostData> => {
    const fullPath = path.join(postsDirectory, `${id}.md`)
    const fileContents = fs.readFileSync(fullPath, 'utf8')

    // Use gray-matter to parse the post metadata section
    const matterResult = matter(fileContents)

    // Use remark to convert markdown into HTML string
    const processor = unified() 
        .use(markdown)
        .use(html)
        .use(stringify)
        .use(sanitize)

    const processedContent = await processor.process(matterResult.content)
    const content = processedContent.contents.toString()
    const date = matterResult.data.date

    // Combine the data with the id
    return {
        date,
        content,
        id,
        ...matterResult.data }}


export const getSortedPostsData = () => {
    // Get file names under /posts
    const fileNames = fs.readdirSync(postsDirectory)
    const allPostsData = fileNames.map(fileName => {
        // Remove ".md" from file name to get id
        const id = fileName.replace(/\.md$/, '')

        // Read markdown file as string
        const fullPath = path.join(postsDirectory, fileName)
        const fileContents = fs.readFileSync(fullPath, 'utf8')

        // Use gray-matter to parse the post metadata section
        const matterResult = matter(fileContents)
        const date = matterResult.data.date

        // Combine the data with the id
        return {
            id,
            date,
            ...matterResult.data }})

    // Sort posts by date
    return allPostsData.sort((a, b) => {
        if (a.date < b.date) {
            return 1
        } else {
            return -1 }})}
