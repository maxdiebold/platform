// lib/about.js

import fs from 'fs'
import html from 'remark-rehype'
import markdown from 'remark-parse'
import matter from 'gray-matter'
import path from 'path'
import sanitize from 'rehype-sanitize'
import stringify from 'rehype-stringify'
import unified from 'unified'


const aboutDirectory = path.join(process.cwd(), 'data')

export const getAboutData = async () => {
  const fullPath = path.join(aboutDirectory, 'about.md')
  const fileContents = fs.readFileSync(fullPath, 'utf8')

  // Use gray-matter to parse the event metadata section
  const matterResult = matter(fileContents)

  // Use remark to convert markdown into HTML string
  const processor = unified() 
    .use(markdown)
    .use(html)
    .use(stringify)
    .use(sanitize)
    
  const processedContent = await processor.process(matterResult.content)
  const contentHtml = processedContent.contents

  // Combine the data with the id
  return {
    contentHtml,
    ...matterResult.data }}

export default getAboutData
