// lib/sendEmail.ts

import { RECIPIENT, SENDER } from "config"
import sgMail from "@sendgrid/mail"

const sendEmail = async ({ name, email, message }) => {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY!)
    let status = 1
    const mail = {
        to: RECIPIENT,
        from: SENDER,
        subject: 'Contact Form: ' + name,
        text: email + '\n' + message,
    }
    try {
        await sgMail.send(mail)
    } catch (error) {
        status = 0
        console.error(error)
        if (error.response) {
            console.error(error.response.body)
        }
    }
    return status
}

export default sendEmail
