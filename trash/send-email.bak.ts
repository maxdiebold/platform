// lib/sendEmail.ts

import { SENDGRID_ENDPOINT, RECIPIENT } from "config"
import fetch from "node-fetch"

export const sendEmail = async ({ name, email, message }) => {
    const response = await fetch(SENDGRID_ENDPOINT, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${process.env.SENDGRID_API_KEY!}`
        },
        body: JSON.stringify({
          personalizations: [
            {
              to: [
                {
                    email: RECIPIENT
                }
              ],
                subject: 'Contact Form: ' + email
            }
          ],
          from: {
            email: 'contact@washingtonplatform.com',
            name: name
          },
          content: [
            {
              type: 'text/plain',
              value: message
            }
          ]
        })
    });
}


export default sendEmail
