---
title: '22nd Annual Crab Carnival'
date: '2021-10-04'
image: '/images/posts/crab-carnival-22.png'
---
Come out of your shell! The Crab Carnival menu will be available from **October 15th** through **November 13th**.  

Enjoy a boatload of fresh & declicious crab fare: Alaskan king crab legs, jonah crab claws, dungeness crab, Florida stone crab claws, snow crab legs, & soft shell blue crabs.

In addition, the Crab Carnival menu will feature a variety of "Crabbetizers", crab soups & salads, and other creations, including the **Crab Grenade**, **Crab Tater Tots**, **Crab Conchiglioni**, and the **Crab Carnage Steamer**.  

You can view the full menu [HERE](https://washingtonplatform.com/docs/crabMenu.pdf).
