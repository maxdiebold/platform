---
title: 'Lobster Dinner Special'
date: '2021-11-11'
time: '11am - 9pm'
thumb: '/images/events/thumbs/lobster-thumb.jpg'
image: '/images/events/thumbs/lobster-thumb.jpg'
---
## $25 Whole Maine Lobster Dinner  

**Every Thursday, all day long!**
