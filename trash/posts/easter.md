---
title: "Easter Brunch & Dinner To Go"
date: '2021-03-11'
image: '/images/posts/last-supper.png'
---
Come by for a special prix fixe, three-course brunch menu this Easter (Sunday, April 4th) from **10am - 3pm**, featuring live jazz with the Mike Sharfe Trio.  

For parties of 5 or more, please call the restaurant at **(513) 421 - 0110** to make a reservation.  

After that, be sure to take dinner home with you! Hot & ready dinners will be available for carry-out from **3pm - 6pm** that evening.  

*Dinners are $40 per party of two, and are available for pre-order only through Friday, April 2nd.*

Our regular menu will not be available on Easter Sunday. For more details, check out the links below:  
[Brunch Menu](https://washingtonplatform.com/docs/easter-brunch.pdf)  
[Dinner To Go Menu](https://washingtonplatform.com/docs/easter-togo.pdf)
