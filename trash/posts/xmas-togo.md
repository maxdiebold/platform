---
title: 'Christmas Dinner To-Go'
date: '2020-12-16'
image: '/images/posts/xmas-turkey.jpg'
---
We've got you covered for the holidays!  
Get a boxed dinner for parties of **two or more**,
all ready to eat!  
It just needs reheating (instructions included).  
Orders will be accepted through **Monday, December 21st**,
and they will be available for pick-up on **Thursday, December 24th from 11am to 3pm**.
Dinners include:
- **Oven-roasted & Hand-carved Turkey & Gravy**
- **Goetta Cornbread Stuffing**
- **Rustic Mashed Potatoes**
- **Brussel Sprouts Almondine**
- **Holiday Fruit & Custard Salad**
- **Yeast Rolls & Honey Butter**
- **Red Velvet Cheesecake**    
  
The meals are **40 per party of two**, and **Oyster Cornbread Stuffing** can be substituted for an additional **5 dollars**.

