---
title: "Pork 'n Oyster Roast"
date: '2021-04-12'
image: '/images/posts/pork-oyster.png'
---
Two of your favorites in one big oven!

Be sure to catch this one day event this year on **Sunday, April 25th**.  

And don't worry -- there are still more oysters where that came from. The Oyster Festival runs until May 8th.

You can check out the full menu [here](https://washingtonplatform.com/docs/oysterMenu.pdf).  

