---
title: 'Thanksgiving Dinner To Go'
date: '2021-11-02'
image: '/images/posts/thanksgiving-to-go.png'
---
We've got you covered for the Thanksgiving!  
Get a boxed dinner for parties of **two or more**,
hot & ready to eat!  
Orders will be accepted through **Monday, November 22st**,
and will be available for pick-up on **Thursday, November 25th from 1:00pm to 5:00pm**.
Dinners include:
- **Oven-roasted & Hand-carved Turkey & Gravy**
- **Goetta Cornbread Stuffing**
- **Rustic Mashed Potatoes**
- **Brussel Sprouts Almondine**
- **Yeast Rolls & Honey Butter**
- **Cranberry Grand Marnier Compote**
- **Pumpkin Cheesecake**
  
The meals are **45 per party of two**, and **Oyster Cornbread Stuffing** can be substituted for an additional **5 dollars**.


