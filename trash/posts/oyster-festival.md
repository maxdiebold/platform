---
title: "35th Annual Oyster Festival"
date: '2021-03-12'
image: '/images/posts/oyster.png'
---
The Oyster Festival is right around the corner, beginning on **March 26**!  

You can check out the full menu [here](https://washingtonplatform.com/docs/oysterMenu.pdf).  

Be sure to catch the **Pork n' Oyster Roast** this time around, one day only on **Sunday, April 25th**.  

More details to follow. To stay in the loop, be sure to [subscribe to our newsletter](https://visitor.r20.constantcontact.com/d.jsp?llr=wqcglzdab&p=oi&m=1103583447574&sit=7bubgkifb&f=c8d466c4-5f66-4eaf-b47d-fbc0a43ad92b).
