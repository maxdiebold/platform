---
title: 'Lobstapalooza IX'
date: '2021-06-09'
image: '/images/posts/lobstapalooza.png'
---
A summer menu that focuses on what we do best: **shellfish!**  

Savor fresh, steamed lobster in a variety of preparations, including:  

- Bourbon-roasted Whole Maine Lobster
- Lobster Curry
- Lobstabellas
- Lobster Bisque
- Champagne Pesto Lobster Tails
- Lobster BLT
- Lobster Poppers
- Lobstocado Salad

And this year, we'll be featuring **$22 Whole Maine Lobster Dinners** every Thursday, *all day long!*  

Check out the full menu [HERE](https://www.washingtonplatform.com/docs/lobstapalooza-menu.pdf)
