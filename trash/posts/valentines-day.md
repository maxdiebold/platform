---
title: "Valentine's Day Dinner"
date: '2021-01-18'
image: '/images/posts/valentine.jpg'
---
Valentine's Day is coming in hot!  

We'll be offering couples' dinners all weekend long, from **February 11th - 14th**, complete with your favorite dishes, wine, and live jazz.

Check out the menu for details, and be sure to call us at **(513) 421 - 0110** or visit [Open Table](https://www.opentable.com/washington-platform-saloon-and-restaurant-reservations-cincinnati?restref=85255&lang=en-US) to make a reservation.
