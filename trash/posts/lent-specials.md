---
title: "Lent Specials!"
date: '2021-02-21'
image: '/images/posts/fish-sandwich.png'
---
# Every Day
## $1 Fish Sandwich  
with the purchase of a second fish sandwich at regular price  
# Every Friday  
## 1/2 Price Fried Fish Special  
with the purchase of another Fried Fish Special at regular price
