---
title: "New Year's Eve 2021"
date: '2020-12-31'
image: '/images/posts/fireworks.jpg'
---
We'll be serving up dinners for 2 on New Year's Eve from 4pm - 9pm, accompanied by live jazz from 6pm - 9pm. Each couple can expect:
- **One Dozen Oysters on the Half Shell**
- **A bottle of Zonin Prosecco**
- **A choice of soup or salad for each guest**
- **An entree choice for each guest:**
    - **Chicken Pompei, with wild rice & quinoa**
    - **Blue Crab Bleu Pasta, with garlic toast**
    - **Crabcake Dinner, with pesto potatoes au gratin**
    - **Salmon Picada, with wild rice & quinoa**
    - **Creole Combo (fried oysters and cajun grilled snapper), with cheese grits**
    - **Grilled Beef Tenderloin, with pesto potatoes au gratin**
- **A dessert flight, including our cheesecake, tiramisu, and chocolate bomb**
  
Reservations are recommended and are available from **4pm - 9pm** on the **31st**.  
100 / couple
