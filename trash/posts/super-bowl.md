---
title: "Super Bowl Sunday"
date: '2021-01-31'
image: '/images/posts/party-subs-wings.png'
---
The Washington Platform will be closing at **6:00 pm** on **Sunday, February 7th**, so the staff can enjoy the Super Bowl!  

Make sure to order your party subs and wings early, so you can enjoy it too!

![football](images/posts/football.png)
