---
title: 'Remus on the River'
date: '2021-05-26'
image: '/images/posts/george-remus.png'
---
Our popular **Murder on the Menu** aboard a
Queen City Riverboat!  
**Saturday Evening, July 17th, 2021**  

Featuring a historically inspired dinner accompanied by cocktails.  

The bootlegger, the trophy wife, and a brief moment of insanity. On
October 6, 1927, George Remus, the “King of the Bootleggers,” shot
his wife dead in front of rush hour traffic and was tried for murder
in one of the most bizarre courtroom dramas in American history.  

**Dinner Menu:**  
- Chicken Cordon Bleu, *stuffed with ham, swiss, & herb butter*  
- Caesar Salad  
- Chive Potatoes  
- Green Beans Almondine  
- Rolls & Butter  

- Strawberry Grand Marnier Trifle  
- Pineapple Upside Down Cake  

- George Remus 'Neat', Manhattan, & Old Pal  

The event – River cruise, entertainment, dinner and George Remus
Whiskey pairings – is an **all-inclusive experience for $75.00**.  
Cruise departs **6:00pm** from Queen City Riverboats,  
**100 O Fallon Ave, Dayton, KY 41074**  
Purchase tickets in advance at [Cincy Murder Dinners](https://cincymurderdinners.com)
