---
title: 'Oktoberfest Menu'
date: '2021-09-14'
image: '/images/posts/oktoberfest-menu.png'
---
Cincinnati will be presenting its Oktoberfest celebrations from September 16 through September 19, but our special menu will be available **all month long!**  

Enjoy a variety of traditional German favorites, and be sure to wash it down with some Oktoberfest beer!

