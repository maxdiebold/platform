// pages/posts/[id].tsx

import Date from 'components/date'
import Layout from 'components/layout'
import { getAllPostIds, getPostData } from 'lib/posts'
import Head from 'next/head'
import React from 'react'

const Post = ({ postData }) => {
  return (
    <Layout>
      <Head>
        <title>{postData.title}</title></Head>
      <article>
        <h1>{postData.title}</h1>
        <div>
          <Date dateString={postData.date} />
        </div>
        <div dangerouslySetInnerHTML={{ __html: postData.contentHtml 
          }} /></article></Layout>)}

const getStaticPaths = async () => {
  const paths = getAllPostIds()
  return {
    paths,
    fallback: false }}

const getStaticProps = async ({ params }) => {
  const postData = await getPostData(params.id)
  return {
    props: {
      postData }}}

export { Post as default, getStaticPaths, getStaticProps }
