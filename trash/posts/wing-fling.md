---
title: 'Wing Fling 2021'
date: '2021-07-29'
image: '/images/posts/wing-menu-back.jpg'
---
The full Wing Fling menu will be available from **Friday, July 30th** to **Saturday, September 4th!**  

Pick:  
- a carrier (**bone-in** or **boneless**  chicken, ***California*** cauliflower, or fried oyster ***Peckers***)  
- a **wet sauce** or a dry-seasoned **_Groaster_**  
- a heat level (**mild**, **medium**, or **_stupid_ hot**)  

You can find the full menu [over on our Menus page](https://www.washingtonplatform.com/docs/wing-menu-front.pdf). If you're a *true* connoisseur, be sure to print it out and track your progress!
