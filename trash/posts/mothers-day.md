---
title: "Mother's Day Brunch"
date: '2021-04-28'
image: ''
---
How's your mother doing? What do you mean you don't know?! Don't you realize Mother's Day is just around the corner?!?!  

We didn't forget. We'll be dishing out a three-course brunch menu, complemented by jazz from **The Mike Sharfe Trio**.  

The brunch menu will be available from **10am - 3pm** on **Sunday, May 9th**. Our regular menu will *not* be available, and we will also be closing that evening so we can honor our own mothers, too.  

You can check out the full brunch menu [here](https://washingtonplatform.com/docs/mothers-brunch.pdf).  

