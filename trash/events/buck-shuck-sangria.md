---
title: 'Cheap Sangria & Cheap Shucks'
date: '2021-12-12'
thumb: '/images/events/thumbs/sangria-thumb.jpeg'
---
Oysters on the half shell for **$1** a piece, all day long!  
And wash 'em down with some sangria, **$5** a glass.  
It's truly a classic pairing.  
  
**Every Sunday!**
