---
title: 'Burgers and Bottles'
date: '2021-12-01'
time: '4pm - 9pm'
thumb: '/images/events/thumbs/fat-boy-thumb.jpeg'
---
Every burger on the menu is **half price!**  
This includes the **Char-grilled** burger, the **Fat Boy** burger, and the **Veggie** burger.  
   
**Bottles of wine** are half off, too.
(applies to any bottle normally priced under $40)  
  
**Every Wednesday!**
