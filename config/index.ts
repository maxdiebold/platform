// config/index.ts

// Stripe configuration
export const CURRENCY = 'usd'
export const MIN_AMOUNT = 10.0
export const MAX_AMOUNT = 5000.0
export const AMOUNT_STEP = 5.0

// Email configuration
export const SENDGRID_ENDPOINT = 'https://api.sendgrid.com/v3/mail/send'
export const RECIPIENT = 'jonvdiebold@yahoo.com'
export const SENDER = 'contact@washingtonplatform.com'

// Home screen configuration
export const GIFT_CARD = '/images/home/giftcard.png'
export const NUMBER_OF_POSTS = 5

// Designate the subdirectory in the 'images' folder
export const SLIDE_DIR = 'slides'

// About page figures
export const IMAGE_1 = "/images/about/canal-near-music-hall.jpg"
export const CAPTION_1 = "View of the Central Parkway canal, with Music Hall on the left"
export const IMAGE_2 = "/images/about/platform-front.jpeg"
export const CAPTION_2 = "Court Street view of the Washington Platform, modern day"

// Google Maps Javascript API
export const GOOGLE_API_KEY='AIzaSyD44htUiiQqctXgELW-S47KcS6ARTXaMac'
export const LOCATION = { lat: 39.106003, lng: -84.517382 }

// Newsletter URL
export const SUBSCRIBE_URL = 'https://visitor.r20.constantcontact.com/d.jsp?llr=wqcglzdab&p=oi&m=1103583447574&sit=7bubgkifb&f=c8d466c4-5f66-4eaf-b47d-fbc0a43ad92b'
