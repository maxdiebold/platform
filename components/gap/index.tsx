// components/gap/index.tsx

export const Gap = ({ height='20vh' }) => {
    return (
        <div style={{ height: height }}>
        </div>
    )
}

export default Gap
