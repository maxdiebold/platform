// components/figure

import styles from "./figure.module.sass"

export const Figure = ({ image, caption }) => {
    return (
        <div className={styles.container}>
            <figure>
                <img src={image} />
            </figure>
            <figcaption>{caption}</figcaption>
        </div>
    )
}

export default Figure
