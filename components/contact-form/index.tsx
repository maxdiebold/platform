// components/contact-form/index.tsx

import styles from "./contact-form.module.sass"
import fetch from "node-fetch"
import React, { useState } from "react"

const SENT = "SENT"
const ERROR = "ERROR"

export const ContactForm = () => {
    const [status, setStatus] = useState('')
    const [state, setState] = useState({
        name: "",
        email: "",
        message: "",
    })

    const handleChange = (event) => {
        const { name, value } = event.target
        setState({
            ...state,
            [name]: value,
        })
    }

    const handlePress = async (e) => {
        e.preventDefault()
        const response = await fetch("/api/send-email", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                name: state.name,
                email: state.email,
                message: state.message,
            }),
        })
        if (response.ok) {
            setStatus(SENT)
            setState({ name: "", email: "", message: "" })
        } else {
            setStatus(ERROR)
        }
    }
    return (
        <div className={styles.container}>
            <div className={styles.field}>
                <label>
                    Name
                    <input name='name' type='text' onChange={handleChange} />
                </label>
            </div>
            <div className={styles.field}>
                <label>
                    Email
                    <input name='email' type='email' onChange={handleChange} />
                </label>
            </div>
            <div className={styles.field}>
                <label>
                    Message
                    <textarea
                        name='message'
                        className={styles.message}
                        placeholder='Leave a message here'
                        onChange={handleChange}
                        value={state.message}
                    />
                </label>
            </div>
            <a href='#' onClick={handlePress}>
                <div className={styles.button}>
                    Send
                </div>
            </a>
            {status == SENT &&
                <p className={styles.sent}>
                    Your message was sent!
                </p>
            }
            {status == ERROR &&
                <p className={styles.error}>
                    Your message couldn't be delivered.
                </p>
            }
        </div>
    )
}

export default ContactForm
