// components/post/index.tsx

import clsx from "clsx"
import Box from "components/horizontal-container"
import styles from "./post.module.sass"
import React from "react"


type Props = {
    title?: string,
    content: string,
    image?: string | null,
    imageAlign?: string,
}


export const Post = ({ title, content, image=null, imageAlign="left" }: Props) => {
    const imageLeft = imageAlign == "left";
    return (
        <div className={styles.container}>
            <Box>
                {image && imageLeft && <img src={image} className={styles.image} />}
                <div className={styles.content}>
                    <h2 className={styles.title}>{title}</h2>
                    <div dangerouslySetInnerHTML={{ __html: content }} />
                </div>
                {image && !imageLeft && <img src={image} className={styles.image} />}
            </Box>
        </div>
    )
}


export default Post
