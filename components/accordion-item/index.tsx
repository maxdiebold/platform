// components/accordion-item/index.js

import React from "react"
import styles from "./accordion-item.module.sass"

export interface Props {
  id?: string,
  name: string,
  price: string,
  description: string
};

export const AccordionItem = ({
    name = "",
    price = "",
    description = "" }: Props): JSX.Element => {
  return (
    <article className={styles.accordionItem}>
      <h4>{name}
        <span className={styles.price}>{price}</span></h4>
      <p>{description}</p>
    </article>)}

export default AccordionItem
