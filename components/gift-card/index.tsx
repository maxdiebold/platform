// components/gift-card/index.tsx

import CheckoutForm from "components/checkout-form"
import styles from "./gift-card.module.sass"

export const GiftCard = ({ cardFront }) => {
    return (
        <div className={styles.container}>
            <h1 className={styles.heading}>
                Purchase a Gift Card
            </h1>
            <img className={styles.image} src={cardFront} />
            <CheckoutForm />
        </div>
    )
}

export default GiftCard
