// components/header/index.tsx

import cn from "classnames"
import styles from "./header.module.sass"
import HomeIcon from "./icons/homeIcon.svg"
import Link from "next/link"
import React, { useState } from "react"
import Collapse from "@kunukn/react-collapse"

export const Header = (): JSX.Element => {
  const [ isOpen, toggleIsOpen ] = useState(false)
  return (
      <header className={styles.header}>
        <div className={styles.bigNav}>
          <Link href='/'><a className={styles.home}>
              <HomeIcon className={styles.homeIcon} />
          </a></Link>
          <Link href='/events'><a className={styles.events}>
            events</a></Link>
          <Link href='/menus'><a className={styles.menus}>
            menus</a></Link>
          <Link href='/about'><a className={styles.about}>
            about</a></Link>
          <Link href='/contact'><a className={styles.contact}>
            contact</a></Link>
          <Link href='/location'><a className={styles.location}>
            location / hours</a></Link></div>
        <div className={styles.mobileNav}>
          <Link href='/'><a className={styles.home}>
              <HomeIcon className={styles.homeIcon} />
          </a></Link>
          <div className={styles.menuBtn}>
            <div onClick={() => toggleIsOpen(!isOpen)}>
              <span></span>
              <span></span>
              <span></span></div>
          </div>
          <Collapse isOpen={isOpen} className={cn(styles.collapseCssTransition, styles.responsiveMenu)}>
            <Link href='/events'><a>events</a></Link>
            <Link href='/menus'><a>menus</a></Link>
            <Link href='/about'><a>about</a></Link>
            <Link href='/contact'><a>contact</a></Link>
            <Link href='/location'><a>location / hours</a></Link></Collapse></div></header>)}

export default Header
