// components/agenda-event/index.tsx

import Date from "components/date"
import styles from "./agenda-event.module.sass"
import Link from "next/link"
import React from "react"

type EventInfo = {
    id: string
    title: string
    date: string
    endDate?: string | null
    time?: string | null
    thumb: string
}

const AgendaEvent = ({ info }: { info: EventInfo }) => {
    const { id, title, date, time, thumb } = info
    let fullDate = <Date dateString={date} />
    if (info.endDate) {
        fullDate = (
            <>
                <Date dateString={date} /> - <Date dateString={info.endDate} />
            </>
        )
    }
    return (
        <li className={styles.event} key={id}>
            <img src={thumb} />
            <section className={styles.details}>
                <Link href={`/events/${id}`}>
                    <a className={styles.title}>{title}</a>
                </Link>
                <div className={styles.lightText}>
                    {fullDate}
                    <p>{time}</p>
                </div>
            </section>
        </li>
    )
}

export default AgendaEvent
