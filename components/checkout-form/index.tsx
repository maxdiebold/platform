// components/checkout-form/index.tsx

import styles from "./checkout-form.module.sass"
import CustomAmountInput from "components/custom-amount-input"
import StripeTestCards from "components/stripe-test-cards"
import { AMOUNT_STEP, CURRENCY, MAX_AMOUNT, MIN_AMOUNT } from "config"
import getStripe from "lib/get-stripe"
import { fetchPostJSON } from "lib/api-helpers"
import { formatAmountForDisplay } from "lib/stripe-helpers"
import React, { useState } from "react"
import Stripe from "stripe"


export const CheckoutForm = () => {
    const [ loading, setLoading ] = useState(false)
    const [ input, setInput ] = useState({
        customAmount: 100 })

    const handleInputChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        setInput({
            ...input,
            [e.currentTarget.name]: e.currentTarget.value })}

    const handleSubmit: React.FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault()
        setLoading(true)
        // Create a Checkout Session.
        const checkoutSession: Stripe.Checkout.Session = await fetchPostJSON('/api/checkout_sessions', {
            amount: input.customAmount })

        if ((checkoutSession as any).statusCode === 500) {
            console.error((checkoutSession as any).message)
            return }

        // Redirect to Checkout.
        const stripe = await getStripe()
        const { error } = await stripe!.redirectToCheckout({
            // Make the id field from the Checkout Session creation API response
            // available to this file, so you can provide it as parameter here
            // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
            sessionId: checkoutSession.id })
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `error.message`.
        console.warn(error.message)
        setLoading(false) }

        return (
            <form className={styles.form} onSubmit={handleSubmit}>
                <CustomAmountInput
                    name={'customAmount'}
                    value={input.customAmount}
                    min={MIN_AMOUNT}
                    max={MAX_AMOUNT}
                    step={AMOUNT_STEP}
                    currency={CURRENCY}
                    onChange={handleInputChange} />
                <button
                    className={styles.button}
                    type="submit"
                    disabled={loading} >
                    Buy {
                        formatAmountForDisplay(input.customAmount, CURRENCY)} Card
                </button></form> )}


export default CheckoutForm
