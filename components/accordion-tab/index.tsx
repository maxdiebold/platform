// components/accordion-tab/index.tsx

import styles from "./accordion-tab.module.sass"
import Accordion from "components/accordion"
import { Props as Item } from "components/accordion-item"
import classNames from "classnames/bind"
import Link from "next/link"
import React from "react"


export type Props = {
    id: string,
    name: string,
    link?: string,
    isChild?: boolean,
    contents: {
        items: Array<Item>,
        tabs: Array<Props> }}

const defaultContents = {
    items: [],
    tabs: [] }

const cn = classNames.bind(styles);

export const AccordionTab = ({ id,
                               name, 
                               link = "", 
                               isChild = false, 
                               contents = defaultContents 
                             }: Props ) => {
    let labelClass = cn({ 
        accordionLabel: true, 
        child: isChild });
    return (
        <section className={styles.accordionTab}>
            <input className={styles.inputCheckbox} id={id} type="checkbox" />
            <label htmlFor={id} className={labelClass}>
                {name}
                {link &&
                    <Link href={link}>
                        <a className={styles.right}>view PDF</a></Link>}</label>
            <Accordion 
                child
                items={contents.items}
                tabs={contents.tabs} /></section>)}

export default AccordionTab
