// components/column/index.tsx
"use-strict"

import styles from "./column.module.sass"
import React from "react"


export const Column = ({ children }) => {
    return (
            <div className={styles.column}>
                {children} </div> )}


export default Column
