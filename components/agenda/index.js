// components/agenda/index.js

import styles from "./agenda.module.sass"
import AgendaEvent from "components/agenda-event"
import React, { useState } from "react"

const Agenda = ({ events }) => {
    const [ size, setSize ] = useState(5);

    const handleClick = (e) => {
        e.preventDefault();
        setSize(size + 5);
    }
    return (
        <>
            <ul className={styles.list}>
                {events.slice(0, size).map((info) => (
                    <AgendaEvent key={info.id} info={info} />
                ))}
            </ul>
            <a 
                className={styles.button}
                href='#' 
                onClick={handleClick}
            >&nbsp; show more &nbsp;</a>
        </>
    )
}

export default Agenda
