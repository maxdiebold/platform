// components/footer/index.tsx

import cn from "classnames"
import Map from "components/map"
import styles from "./footer.module.sass"
import { Facebook, OpenTable, Twitter, YouTube } from "./icons"
import Link from "next/link"
import React, { ReactNode } from "react"

const Footer = () => {
    return (
        <footer className={styles.footer}>
            <div className={styles.location}>
                1000 Elm St <br />
                Cincinnati, OH
                <br />
                45202 <br />
                <br />
                Sunday
                <br />
                <span className={styles.hours}>&nbsp;&nbsp;11am to 9pm</span>
                <br />
                Monday
                <br />
                <span className={styles.hours}>&nbsp;&nbsp;closed</span>
                <br />
                Tues
                <br />
                <span className={styles.hours}>&nbsp;&nbsp;closed</span>
                <br />
                Wed <br />
                <span className={styles.hours}>&nbsp;&nbsp;11am to 9pm</span>
                <br />
                Thurs
                <br />
                <span className={styles.hours}>&nbsp;&nbsp;11am to 9pm</span>
                <br />
                Fri & Sat
                <br />
                <span className={styles.hours}>&nbsp;&nbsp;11am to 10pm</span>
                <br />
                <span className={styles.hours}>&nbsp;&nbsp;(bar open until 11pm)</span>
            </div>
            <div id='reserve' className={styles.reserve}>
                <a
                    href='https://www.opentable.com/washington-platform-saloon-and-restaurant-reservations-cincinnati?restref=85255&lang=en-US'
                    target='_blank'>
                    <button type='button' className={styles.otButton}>
                        Reserve
                    </button>
                    <OpenTable className={styles.opentable} />
                </a>
            </div>
            <div className={styles.connect}>
                <a href='https://twitter.com/washplat' target='_blank'>
                    <Twitter className={cn(styles.icon, styles.twitter)} />
                </a>
                <a
                    href='https://www.facebook.com/Washington-Platform-Saloon-Restaurant-216298142728'
                    target='_blank'>
                    <Facebook className={cn(styles.icon, styles.facebook)} />
                </a>
                <a href='https://www.youtube.com/playlist?list=PLKh3vwtP5uD_VnRlsNnP_YOgSAo9slhQj'>
                    <YouTube className={cn(styles.icon, styles.youtube)} />
                </a>
            </div>
            <div className={styles.phone}>
                513 - 421 - 0110 <br />
            </div>
            <div className={styles.copyright}>&copy;2020 J Max Diebold</div>
        </footer>
    )
}

export default Footer
