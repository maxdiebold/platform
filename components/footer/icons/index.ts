// components/footer/icons

export { default as Facebook } from "./facebook.svg"
export { default as OpenTable } from "./openTable.svg"
export { default as Twitter } from "./twitter.svg"
export { default as YouTube } from "./youTube.svg"
