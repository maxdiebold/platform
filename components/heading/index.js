// components/heading/index.js

import classNames from "classnames/bind"
import styles from "./heading.module.sass"
import React from "react"

const cn = classNames.bind(styles)

export const Heading = ({ align="center", children, width='auto'}) => {
    const headingClass = cn({
        heading: true,
        left: align == 'left',
        center: align == 'center',
        right: align == 'right',
    })
    return (
        <section 
            className={headingClass}
            style={{width: width}}
        >
            {children}
        </section>
    )
}

export default Heading
