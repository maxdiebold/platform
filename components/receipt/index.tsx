// components/receipt/index.tsx

import Link from "next/link"
import React from "react"
import styles from "./receipt.module.sass"

export const Receipt = ({ status, id }) => {
    return (
        <div className={styles.container}>
            <h1 className={styles.heading}>Payment Receipt</h1>
            <h2 className={styles.status}>Status: {status}</h2>
            <p>
                Your Checkout Session ID:{" "}
                <code>{id}</code>
            </p>
            <p>
                <Link href='/'>
                    <a>Go home</a>
                </Link>
            </p>
        </div>
    )
}

export default Receipt
