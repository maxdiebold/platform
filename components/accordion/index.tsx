// components/accordion/index.tsx
"use-strict"

import AccordionItem from "components/accordion-item"
import AccordionTab from "components/accordion-tab"
import classNames from "classnames/bind"
import styles from "./accordion.module.sass"
import React from "react"
import { Props as Tab } from "components/accordion-tab"
import { Props as Item } from "components/accordion-item"

const cn = classNames.bind(styles)

export interface Props {
    child?: boolean,
    items?: Array<Item>,
    tabs?: Array<Tab> }

export const Accordion = ({ child = false, items = [], tabs = [] }: Props): JSX.Element => {
    let accordionClass = cn({
        accordion: !child,
        accordionChild: child });
    return (
        <section className={accordionClass}>
            {items.map( item => (
                <AccordionItem 
                    key={item.id}
                    name={item.name}
                    price={item.price}
                    description={item.description} />
                                ))} 
            {tabs.map( tab => (
                <AccordionTab 
                  isChild={child}
                  id={tab.id}
                  key={tab.id}
                  name={tab.name}
                  link={tab.link ? tab.link : ''}
                  contents={tab.contents} /> ))}
        </section> )}

export default Accordion
