// components/marker

import styles from "./marker.module.sass"

type Props = {
    lat: number,
    lng: number,
    text: string,
}

export const Marker = (props: Props) => {
    return (
        <div className={styles.marker}></div>
    )
}

export default Marker
