// components/splash/index.tsx

import Logo from "./icons/logo.svg"
import React from "react"
import styles from "./splash.module.sass"

const Splash = () => {
    return (
        <section className={styles.background}>
            <Logo className={styles.logo} />
        </section>
    )
}

export default Splash
