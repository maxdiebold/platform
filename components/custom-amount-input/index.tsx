// components/custom-amount-input/index.tsx

import { formatAmountForDisplay } from 'lib/stripe-helpers'
import React from 'react'
import styles from "./custom-amount-input.module.sass"


type Props = {
  name: string
  value: number
  min: number
  max: number
  currency: string
  step: number
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  className?: string
}

export const CustomAmountInput = ({
  name,
  value,
  min,
  max,
  currency,
  step,
  onChange,
  className,
}: Props) => (
    <label className={styles.label}>
        Custom amount ({formatAmountForDisplay(min, currency)}-
        {formatAmountForDisplay(max, currency)}):
        <input
            className={styles.number}
            type="number"
            name={name}
            value={value}
            min={min}
            max={max}
            step={step}
            onChange={onChange}></input>
        <input
            className={styles.range}
            type="range"
            name={name}
            value={value}
            min={min}
            max={max}
            step={step}
            onChange={onChange}></input></label> )


export default CustomAmountInput
