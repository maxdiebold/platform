// components/event/index.tsx

import Date from "components/date"
import Gap from "components/gap"
import styles from "./event.module.sass"

type Props = {
    title: string
    date: string
    endDate?: string | null
    image?: string | null
    time?: string | null
    contentHtml: string
}

export const Event = ({
    title,
    date,
    endDate,
    time,
    image,
    contentHtml,
}: Props) => {
    let fullDate = <Date dateString={date} />
    if (endDate) {
        fullDate = (
            <>
                <Date dateString={date} /> - <Date dateString={endDate} />
            </>
        )
    }
    return (
        <div className={styles.container}>
            <h1 className={styles.heading}>{title}</h1>
            <div className={styles.date}>
                {fullDate}
                {time && <p>{time}</p>}
            </div>
            <div
                className={styles.content}
                dangerouslySetInnerHTML={{ __html: contentHtml }}
            />
            {image && <img className={styles.image} src={image} />}
            <Gap height='20vh' />
        </div>
    )
}

export default Event
