// components/map

import Marker from "components/marker"
import { GOOGLE_API_KEY, LOCATION } from "config"
import GoogleMapReact from "google-map-react"
import styles from "./map.module.sass"

export const Map = () => {
    return (
        <div className={styles.map}>
            <GoogleMapReact
                bootstrapURLKeys={{ key: GOOGLE_API_KEY }}
                defaultCenter={LOCATION}
                defaultZoom={17}
            >
                <Marker lat={LOCATION.lat} lng={LOCATION.lng} text=''/>
            </GoogleMapReact>
        </div>
    )
}

export default Map
