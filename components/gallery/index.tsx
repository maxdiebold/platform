// components/gallery

import styles from "./gallery.module.sass"

type Props = {
    slides: string[],
}

export const Gallery = ({ slides }) => {
    return (
        <div className={styles.gallery}>
            {slides && slides.map((slide, index) => (
                <img
                    src={slide}
                    key={index}
                />
            ))}
        </div>
    )
}

export default Gallery
