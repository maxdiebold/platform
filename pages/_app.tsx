import Link from 'next/link'
import 'styles/global.sass'

const Platform = ({ Component, pageProps }) => {
  return <Component {...pageProps} />
}

export default Platform
