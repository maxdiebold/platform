// pages/menus.tsx
"use-strict"

import Accordion from "components/accordion"
import Gap from "components/gap"
import Heading from "components/heading"
import Layout from "components/layout"
import { getMenusData } from "lib/menus"
import React from "react"
import { Props as Item } from "components/accordion-item"
import { Props as Tab } from "components/accordion-tab"


interface MenuData {
    child?: boolean,
    contents: {
        items: Array<Item>,
        tabs: Array<Tab> },
    id: string,
    name: string,
    link?: string }

type StaticProps = {
    props: Props }

type Props = {
    menusData: MenusData }

type MenusData = Array<MenuData>

export const Menus = ({ menusData }: Props ): JSX.Element => {
    return (
        <Layout title='Washington Platform Saloon & Restaurant'>
            <Heading>Menus</Heading>
            <Accordion tabs={menusData} />
            <Gap height='40vh' />
        </Layout>
    )
}

export const getStaticProps = async (): Promise<StaticProps> => {
    const unsortedMenusData = getMenusData();
    let menusData: any[] = []
    Object.keys(unsortedMenusData).sort().forEach( key => {
        menusData = [...menusData, unsortedMenusData[key]] })
    return {
        props: {
            menusData }}}

export default Menus
