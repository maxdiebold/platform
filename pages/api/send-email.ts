import sendEmail from "lib/send-email"
import { NextApiRequest, NextApiResponse } from "next"

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === "POST") {
        const { name, email, message } = req.body
        const status = await sendEmail({ name, email, message })
        if (status) {
            return res.status(200).end()
        } 
    }
    return res.status(404).json({
        error: {
            code: "not_found",
            message:
                "The requested endpoint was not found or doesn't support this method.",
        },
    })
}

export default handler
