// pages/index.tsx

import Heading from "components/heading"
import Gallery from "components/gallery"
import GiftCard from "components/gift-card"
import HorizontalContainer from "components/horizontal-container"
import Layout from "components/layout"
import Post from "components/post"
import Splash from "components/splash"
import { GIFT_CARD, NUMBER_OF_POSTS, SLIDE_DIR, SUBSCRIBE_URL } from "config"
import { getSlides } from "lib/get-slides"
import { getAllPosts, PostData } from "lib/posts"
import Link from "next/link"
import styles from "styles/utils.module.sass"

type Props = {
    allPosts: PostData[],
    allSlides: string[],
}

export const Home = ({ allPosts, allSlides }: Props) => {
    const numberOfPosts = allPosts.length > NUMBER_OF_POSTS 
        ? NUMBER_OF_POSTS : allPosts.length
    let alignLeft = true
    let posts: JSX.Element[] = []
    for (let i = 0; i < numberOfPosts; i++) {
        posts.push(
            <Post
                title={allPosts[i].title}
                content={allPosts[i].content}
                image={allPosts[i].image}
                imageAlign={alignLeft ? "left" : "right"}
                key={allPosts[i].id}
            />
        )
        alignLeft = !alignLeft
    }
    return (
        <Layout home title='Washington Platform Saloon & Restaurant'>
            <Splash />
            <HorizontalContainer>
                <Heading>
                    <p className={styles.mobileHidden}>shuck 'em</p>
                    <p className={styles.mobileHidden}>suck 'em</p>
                    <p className={styles.mobileHidden}>eat 'em raw</p>
                </Heading>
                {/*<GiftCard cardFront={GIFT_CARD} />*/}
            </HorizontalContainer>
            {posts}
            <Heading align='right' width='90%'>
                <Link href='/events'><a>
                    ...and all that jazz
                </a></Link>
            </Heading>
            <Gallery slides={allSlides} />
            <Heading>
                <a href={SUBSCRIBE_URL} target="_blank">
                    subscribe to our newsletter
                </a>
            </Heading>
        </Layout>
    )
}

export const getStaticProps = async () => {
    const allPosts = await getAllPosts()
    const allSlides = getSlides(SLIDE_DIR)
    return {
        props: {
            allPosts,
            allSlides,
        },
    }
}

export default Home
