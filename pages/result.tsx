// pages/result.tsx

import { NextPage } from "next"
import { useRouter } from "next/router"
import Layout from "components/layout"
import PrintObject from "components/print-object"
import Receipt from "components/receipt"
import { fetchGetJSON } from "lib/api-helpers"
import useSWR from "swr"

const ResultPage: NextPage = () => {
    const router = useRouter()

    // Fetch CheckoutSession from static page via
    // https://nextjs.org/docs/basic-features/data-fetching#static-generation
    const { data, error } = useSWR(
        router.query.session_id
            ? `/api/checkout_sessions/${router.query.session_id}`
            : null,
        fetchGetJSON
    )

    const status = data?.payment_intent?.status ?? "loading..."
    const id = router.query.session_id ?? "loading..."

    if (error) return <div>failed to load</div>

    return (
        <Layout title='Checkout Payment Result'>
            <Receipt status={status} id={id} />
        </Layout>
    )
}

export default ResultPage
