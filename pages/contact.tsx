// pages/contact.tsx

import ContactForm from "components/contact-form"
import Gap from "components/gap"
import Heading from "components/heading"
import Layout from "components/layout"
import { SUBSCRIBE_URL } from "config"
import React from "react"


export const Contact = () => {
  return (
    <Layout title='Contact us'>
        <Heading>Contact</Heading>
        <ContactForm />
        <Heading>
            <a href={SUBSCRIBE_URL} target="_blank">
                subscribe to our newsletter
            </a>
        </Heading>
        <Gap />
    </Layout>
  )
}


export default Contact
