// pages/about.tsx

import Column from "components/column"
import Figure from "components/figure"
import Gap from "components/gap"
import Heading from "components/heading"
import Layout from "components/layout"
import { IMAGE_1, IMAGE_2, CAPTION_1, CAPTION_2 } from "config"
import getAboutData from "lib/about"
import React from "react"

export const About = ({ aboutData }) => {
    const { contentHtml } = aboutData
    return (
        <Layout>
            <Heading>About</Heading>
            <Column>
                <Figure image={IMAGE_1} caption={CAPTION_1} />
                <div dangerouslySetInnerHTML={{ __html: contentHtml }} />
                <Figure image={IMAGE_2} caption={CAPTION_2} />
            </Column>
            <Gap height='20vh' />
        </Layout>
    )
}

export const getStaticProps = async () => {
  const aboutData = await getAboutData()
  return {
    props: {
      aboutData }}}

export default About
