// pages/location.tsx

import Layout from "components/layout"
import Map from "components/map"
import React from "react"


export const Location = () => {
    return (
        <Layout>
            <Map />
        </Layout> 
    )
}

export default Location
