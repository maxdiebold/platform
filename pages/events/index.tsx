// pages/events/index.tsx

import Agenda from "components/agenda"
import Heading from "components/heading"
import Layout from "components/layout"
import { getSortedEventsData } from "lib/events"
import React from "react"

interface EventsData {
  id: string,
  date: string,
  endDate?: string | null,
  title: string }

export const Events = ({ allEventsData }: { allEventsData: Array<EventsData> }) => {
  return (
    <Layout title='Jazz & Events - Washington Platform Saloon & Restaurant'>
      <Heading>Jazz & Events</Heading>
      <Agenda events={allEventsData} /></Layout>)}

export const getStaticProps = async () => {
  const allEventsData = getSortedEventsData()
  return {
    props: {
      allEventsData }}}


export default Events
