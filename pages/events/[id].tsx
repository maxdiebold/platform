// pages/events/[id].tsx

import Event from "components/event"
import Layout from "components/layout"
import { getAllEventIds, getEventData } from "lib/events"
import React from "react"

export const EventPage = ({ eventData }) => {
    const { title, date, contentHtml } = eventData
    return (
        <Layout title={title}>
            <Event
                time={eventData.time ?? null}
                title={title}
                date={date}
                endDate={eventData.endDate ?? null}
                image={eventData.image ?? null}
                contentHtml={contentHtml}
            />
        </Layout>
    )
}

export const getStaticPaths = async () => {
    const paths = getAllEventIds()
    return {
        paths,
        fallback: false,
    }
}

export const getStaticProps = async ({ params }) => {
    const eventData = await getEventData(params.id)
    return {
        props: {
            eventData,
        },
    }
}

export default EventPage
