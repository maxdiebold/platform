---
title: 'Kristopher Keith (flute & sax), Brad Myers (guitar), and Michael Sharfe (bass)'
date: '2021-04-23'
time: '7pm - 10pm'
thumb: '/images/events/thumbs/kristopher-thumb.png'
---
