---
title: 'The EWI Quartet'
date: '2021-07-16'
time: '8pm - 11pm'
thumb: '/images/events/thumbs/ewi-thumb.png'
image: '/images/events/ewi.png'
---
Featuring  
**Eric Lechliter** on the EWI (Electronic Wind Instrument), **Michael Sharfe** on bass, 
**George Simon** on guitar, and **Phil Tipton** on drums
