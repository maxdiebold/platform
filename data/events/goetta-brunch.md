---
title: 'Goetta Brunch w/ Vintage Vibe Trio'
date: '2021-03-07'
time: '11:00am - 3:00pm'
thumb: '/images/events/thumbs/phil-degreg-thumb.png'
image: '/images/events/goetta-brunch.png'
---
Soak up Saturday night's beer with some brunch... and more beer!  
We'll have a Bockfest-themed brunch from **11am - 3pm**, with live music accompaniment by the  
**Vintage Vibe Trio**. The menu includes:  
