---
title: 'The Feast of the Seven Fishes'
date: '2021-12-23'
endDate: '2021-12-24'
time: '4pm - 9pm'
image: '/images/events/seven-fishes.png'
thumb: '/images/events/thumbs/seven-fishes-thumb.jpeg'
---
A traditional 7-course extravaganza featuring all of the best seafood  
and holiday jazz from the **Second Line Jazz Trio**.  
Reservations are required and are available from **4pm - 9pm** on the **23rd** and the **24th**.  

Here's last year's menu to tempt you:  
**Oysters on the 1/2 Shell**  
**Chilled Shrimp Louise**  
**Seafood Bisque**  
**Calamari Caesar Salad**  
**Sole Almondine**  
**Linguini, Mussels, & Clams in a Pesto Cream**  
**Crab-stuffed Salmon with Hollandaise**  
**Canoli**   

