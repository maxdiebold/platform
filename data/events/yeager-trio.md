---
title: 'The Wayne Yeager Trio'
date: '2021-07-23'
time: '8pm - 11pm'
thumb: '/images/events/thumbs/wayne-yeager-thumb.png'
image: '/images/events/wayne-yeager.png'
---
Featuring  
**Wayne Yeager** on organ, **Bob Roetker** on guitar, & **Melvin Broach** on drums
