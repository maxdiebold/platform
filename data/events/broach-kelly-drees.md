---
title: 'Pat Kelly Organ Trio'
date: '2021-11-05'
time: '7:30pm - 10:30pm'
image: '/images/events/broach-kelly-drees.png'
thumb: '/images/events/thumbs/pat-kelly-thumb.png'
---
**Pat Kelly** on organ, with  
**Dan Drees** on tenor sax, and  
**Melving Broach** on drums
