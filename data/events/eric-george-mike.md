---
title: 'Eric Lechliter (trumpet / EWI), George Simon (guitar), & Michael Sharfe (bass)'
date: '2021-02-26'
time: '7pm - 9pm'
thumb: '/images/events/thumbs/eric-lechliter_thumb.png'
image: '/images/events/mike-eric-george.png'
---
