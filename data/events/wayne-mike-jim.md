---
title: 'The Wayne Yeager Trio w/ Michael Sharfe (bass) & Jim Leslie (drums)'
date: '2021-03-26'
time: '7pm - 10pm'
thumb: '/images/events/thumbs/mike-thumb.png'
image: ''
---
Featuring Wayne Yeager on piano and joined by Michael Sharfe on bass and Jim Leslie on drums.

