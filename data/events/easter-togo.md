---
title: 'Easter Dinner To Go'
date: '2021-04-04'
time: '3:00pm - 6:00pm'
thumb: '/images/events/thumbs/easter-thumb.png'
image: '/images/events/easter-togo.png'
---
Hot & ready dinners will be available for carry-out from **3pm - 6pm** on Easter Sunday!

Dinners are $40 per party of two, and are available for pre-order only through Friday, April 2nd.

*Regular menu will not be available on Easter Sunday.*
