---
title: 'Mandy Gaines (vocals), Mike Sharfe (bass), & Brad Meyers (guitar)'
date: '2021-04-17'
time: '7pm - 10pm'
thumb: '/images/events/thumbs/mandy-gaines_thumb.png'
image: '/images/events/mandy-gaines.png'
---

