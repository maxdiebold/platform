---
title: 'Pat Kelly Organ Trio with Greg Chako (guitar) & Jim Leslie (drums)'
date: '2021-05-01'
time: '7pm - 10pm'
thumb: '/images/events/pat-kelly.png'
image: '/images/events/pat-kelly.png'
---
