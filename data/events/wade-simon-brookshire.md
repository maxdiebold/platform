---
title: 'Mike Wade w/ George Simon & Eddie Brookshire'
date: '2021-08-28'
time: '7:30pm - 10:30pm'
image: '/images/events/mike-wade.png'
thumb: '/images/events/thumbs/mike-wade-thumb.png'
---
Featuring  
**Mike Wade** on trumpet, with  
**George Simon** on guitar, &  
**Eddie Brookshire** on bass
