---
title: 'The Bobby Sharp Trio'
date: '2021-11-20'
time: '7:30pm - 10:30pm'
image: '/images/events/simon-sharp-cashwell.png'
thumb: '/images/events/thumbs/bobby-sharp-thumb.png'
---
**Bobby Sharp** on drums, with  
**George Simon** on guitar, and  
**Brian Cashwell** on piano
