---
title: 'Andrea Cefalo (vocals) & Brian Cashwell (piano)'
date: '2021-06-11'
time: '8pm - 11pm'
thumb: '/images/events/thumbs/andrea-cefalo-thumb.png'
image: '/images/events/andrea-cefalo.png'
---

