---
title: 'Vocalist Dan "Ole Green Eyes" Radank, w/ Brian Batchelor-Glader & Marc Wolfley'
date: '2021-08-21'
time: '7:30pm - 10:30pm'
thumb: '/images/events/thumbs/dan-radank-thumb.png'
image: '/images/events/dan-radank.png'
---
### Featuring:  
Vocalist **Dan Radank**, AKA "Ole Green Eyes",  
accompanied by  
**Brian Batchelor-Glader** on piano  
&  
**Marc Wolfley** on drums

