---
title: 'Saxophonist Phil Hilger'
date: '2021-12-03'
time: '7:30pm - 10:30pm'
image: '/images/events/phil-hilger.png'
thumb: '/images/events/thumbs/phil-hilger-thumb.jpg'
---
Saxophonist **Phil Hilger**, with  
**Michael Sharfe** on bass, and  
**John Zappa** on drums
