---
title: 'Vocalist Kelly Ginn & Brian Cashwell'
date: '2021-10-01'
time: '7:30pm - 10:30pm'
image: '/images/events/kelly-brian.png'
thumb: '/images/events/thumbs/kelly-ginn-thumb.png'
---
Featuring  
Vocalist **Kelly Ginn** with  
**Brian Cashwell** on piano
