---
title: 'Vocalist Pamela Mallory & Trio'
date: '2021-07-24'
time: '8pm - 11pm'
thumb: '/images/events/thumbs/pam-mallory-thumb.png'
image: '/images/events/pam-mallory-2.png'
---
Featuring  
Vocalist **Pamela Mallory**, with **Wayne Yeager** on piano, **Michael Sharfe** on bass, and **Melvin Broach** on drums
