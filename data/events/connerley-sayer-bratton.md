---
title: 'The Jim Connerley Trio'
date: '2021-11-26'
time: '7:30pm - 10:30pm'
image: '/images/events/jim-connerley.png'
thumb: '/images/events/thumbs/jim-thumb.png'
---
**Jim Connerley** on piano, with  
**Eric Sayer** on bass, and  
**Ben Bratton** on drums
