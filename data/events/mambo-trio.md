---
title: 'The Mambo Trio'
date: '2021-02-20'
time: '7pm - 9pm'
thumb: '/images/events/thumbs/mike-thumb.jpeg'
image: '/images/events/mambo-trio.jpeg'
---
The Mambo Trio is **Brian Batchelor-Glader** on piano, **Michael Sharfe** on bass, and **Phil Tipton** on drums.  
Come out for some hot latin jazz!
