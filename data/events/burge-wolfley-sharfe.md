---
title: 'Rusty Burge, Michael Sharfe, & Marc Wolfley'
date: '2021-07-02'
time: '8pm - 11pm'
thumb: '/images/events/thumbs/rusty-burge-thumb.png'
image: '/images/events/wolfley-sharfe-burge.png'
---
Featuring  
**Rusty Burge** on vibes, **Michael Sharfe** on bass, and **Marc Wolfley** on drums
