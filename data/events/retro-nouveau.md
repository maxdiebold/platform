---
title: "The Retro Nouveau Quartet"
date: '2021-12-17'
time: '7:30pm - 10:30pm'
image: '/images/events/retro-nouveau.png'
thumb: '/images/events/thumbs/retro-nouveau-thumb.jpg'
---
Featuring compositions from their CD *"Live at the Greenwich"*  

With **Josh Kline** on tenor sax,  
**Dan Karlsberg** on piano,  
**Michael Sharfe** on bass, &  
**Melvin Broach** on drums
