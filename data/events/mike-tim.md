---
title: 'Tim McCord (sax & flute) & Michael Sharfe (bass)'
date: '2021-04-09'
time: '7pm - 10pm'
thumb: '/images/events/thumbs/tim-mccord_thumb.png'
image: '/images/events/mike-tim.png'
---
