---
title: 'The Feast of the Seven Fishes'
date: '2020-12-24'
time: '4pm - 9pm'
thumb: '/images/events/thumbs/seven-fishes-thumb.jpeg'
image: '/images/events/seven-fishes.png'
---
A traditional 7-course extravaganza featuring all of the best seafood and holiday jazz from the **Second Line Jazz Trio**.  
  
**Oysters on the 1/2 Shell**  
**Chilled Shrimp Louise**  
**Seafood Bisque**  
**Calamari Caesar Salad**  
**Sole Almondine**  
**Linguini, Mussels, & Clams in a Pesto Cream**  
**Crab-stuffed Salmon with Hollandaise**  
**Canoli**  
  
Reservations are required and are available from **4pm - 9pm** on the **23rd** and the **24th**.  
**$55** per person, or **$80** with wine pairings.  
