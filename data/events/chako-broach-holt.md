---
title: 'The Greg Chako Guitar Trio'
date: '2021-09-03'
time: '7:30pm - 10:30pm'
image: '/images/events/greg-chako.png'
thumb: '/images/events/thumbs/greg-chako-thumb.png'
---
Featuring  
**Greg Chako** on guitar, with  
**Melvin Broach** on drums, &  
**Matt Holt** on bass  

Come out and enjoy jazz with your dinner!  
