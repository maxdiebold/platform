---
title: 'Brad Myers & Michael Sharfe w/ special guest Dan Dorff'
date: '2021-06-18'
time: '8pm - 11pm'
thumb: '/images/events/thumbs/brad-mike-thumb.png'
image: '/images/events/brad-mike.png'
---
Featuring  
**Brad Myers** on guitar, **Michael Sharfe** on bass, and special guest **Dan Dorff** on drums
