---
title: 'Vocalist Pam Mallory Trio'
date: '2021-06-04'
time: '8pm - 11pm'
thumb: '/images/events/thumbs/pam-mallory-thumb.png'
image: '/images/events/pam-mallory.png'
---
Featuring  
**Pam Mallory** on vocals, with Wayne Yeager on piano, Michael Sharfe on bass, and Jim Leslie on drums.  

Come help us unwrap Pam's **new CD**, titled *True Colors*, featuring all original art work!
