---
title: 'The Brian Cashwell Trio'
date: '2021-11-27'
time: '7:30pm - 10:30pm'
image: '/images/events/sharfe-cashwell-leslie.png'
thumb: '/images/events/thumbs/brian-cashwell-thumb.png'
---
**Brian Cashwell** on piano, with  
**Michael Sharfe** on bass, and  
**Jim Leslie** on drums
