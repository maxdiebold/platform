---
title: 'Trombonist Marc Fields w/ Dan Karlsberg & Melvin Broach'
date: '2021-08-20'
time: '7:30pm - 10:30pm'
image: '/images/events/marc-fields.png'
thumb: '/images/events/thumbs/marc-fields-thumb.png'
---
### Featuring:
**Marc Fields** on trombone,  
**Dan Karlsberg** on organ,  
&  
**Melvin Broach** on drums
