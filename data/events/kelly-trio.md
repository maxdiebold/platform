---
title: 'The Pat Kelly Organ Trio'
date: '2021-07-17'
time: '8pm - 11pm'
thumb: '/images/events/pat-kelly.png'
image: '/images/events/pat-kelly.png'
---
Featuring  
**Pat Kelly** on organ, **Greg Chako** on guitar, & **Melvin Broach** on drums
