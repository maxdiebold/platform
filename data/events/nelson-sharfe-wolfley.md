---
title: 'Taylor Nelson, Michael Sharfe, & Marc Wolfley'
date: '2021-11-19'
time: '7:30pm - 10:30pm'
image: '/images/events/sharfe-nelson-wolfley.png'
thumb: '/images/events/thumbs/taylor-nelson-thumb.png'
---
We're welcoming a **new member** to the Cincy Jazz scene!  

**Taylor Nelson** is a guitarist, composer, and educator based in Cincinnati, Ohio.  
Originally from Chelsea, Michigan, Taylor has been performing and teaching throughout
the Midwest for over a decade. She is the Adjunct Professor of Jazz Guitar at the
University of Dayton, where she teaches jazz and modern guitar and plays with the
University of Dayton Faculty Jazztet.  

She has performed with many world-renowned and regional musicians such as Doc Severinsen, Tamir
Hendelman, Adam Nussbaum, Don Braden, Mike Stern, Toledo Symphony Orchestra,
David Bixler, Jeff Halsey, Tyrone Wheeler’s 502 Vintage Keys, Nomad Quartet, Carly
Johnson, Chris Fitzgerald, Dave Clark, Kendall Carter, Mike Hyman, Gabe Evens,
Hamilton Pinheiro, and the Cincinnati Contemporary Jazz Orchestra.
