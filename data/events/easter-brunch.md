---
title: 'Easter Jazz Brunch'
date: '2021-04-04'
time: '10:00am - 3:00pm'
thumb: '/images/events/thumbs/easter-thumb.png'
image: '/images/events/easter-brunch.png'
---
Enjoy a special prix fixe, three course brunch, including traditional, breakfast & lunch favorites.  

Featuring Live Jazz with the **Mike Sharfe Trio**.  

Fresh oysters, full bar, wine list & Craft Beers available. Parties of 5 or more please call the restaurant to make your reservation.
