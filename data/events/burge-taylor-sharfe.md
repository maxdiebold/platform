---
title: 'The Options Jazz Trio'
date: '2021-05-28'
time: '7pm - 10pm'
image: '/images/events/burge-taylor-sharfe.png'
thumb: '/images/events/thumbs/mike-thumb.png'
---
Rusty Burge on vibes, John Taylor on drums, & Michael Sharfe on bass
