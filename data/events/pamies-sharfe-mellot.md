---
title: 'The Sergio Pamies Trio'
date: '2021-06-25'
time: '8pm - 11pm'
thumb: '/images/events/thumbs/sergio-pamies-thumb.png'
image: '/images/events/sergio-pamies.png'
---
Featuring  
**Sergio Pamies** on piano, **Michael Sharfe** on bass, & **Jeff Mellot** on drums
