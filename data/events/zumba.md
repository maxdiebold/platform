---
title: 'The Zumba Band'
date: '2021-01-01'
time: '8pm - 11pm'
image: '/images/events/zumba.png'
thumb: '/images/events/thumbs/zumba-thumb.png'
---
## Fun Latin Music!
