---
title: 'Rusty Burge (vibes) & Michael Sharfe (bass)'
date: '2021-02-27'
time: '7pm - 9pm'
thumb: '/images/events/thumbs/rusty-burge_thumb.png'
image: '/images/events/rusty-mike.png'
---
