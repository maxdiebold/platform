---
title: 'The Kris Keith Quartet'
date: '2021-09-17'
time: '7:30pm - 10:30pm'
image: '/images/events/kris-keith.png'
thumb: '/images/events/thumbs/kris-keith-thumb.png'
---
Featuring world-class flute and sax player  
**Kris Keith**  
with **Brian Batchelor-Glader** on piano,  
**Michael Sharfe** on bass, &  
**Jeff Mellott** on drums

