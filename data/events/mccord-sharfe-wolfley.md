---
title: 'Tim McCord Sax Trio'
date: '2021-11-06'
time: '7:30pm - 10:30pm'
image: '/images/events/wolfley-mccord-sharfe.png'
thumb: '/images/events/thumbs/tim-mccord-thumb.png'
---
**Tim McCord** on sax, with  
**Michael Sharfe** on bass, and  
**Marc Wolfley** on drums
