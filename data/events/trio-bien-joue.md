---
title: 'Trio Bien Joue'
date: '2021-06-26'
time: '8pm - 11pm'
thumb: '/images/events/thumbs/trio-bien-joue-thumb.png'
image: '/images/events/trio-bien-joue.png'
---
Featuring  
**Lou Lausche** on bass, **Wayne Yeager** on piano, & **Bob Roetker** on guitar
