---
title: "Dan \"ol' Green Eyes\" Radank"
date: '2021-12-04'
time: '7:30pm - 10:30pm'
image: '/images/events/dan-radank.png'
thumb: '/images/events/thumbs/dan-radank-thumb.png'
---
Featuring the holiday sounds of Sinatra & more!  

**Dan Radank**, a.k.a. "ol' Green Eyes", on vocals, with  
**Brian Batchelor Glader** on piano, and  
**Tim McCord** on saxophone & flute
