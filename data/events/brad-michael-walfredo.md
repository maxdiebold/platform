---
title: 'Brad Myers, Michael Sharfe, & Walfredo Reyes Jr.'
date: '2021-05-08'
time: '7pm - 10pm'
thumb: '/images/events/thumbs/brad-mike-thumb.png'
image: '/images/events/brad-mike.png'
---
Featuring Brad Myers on guitar, Michael Sharfe on bass, and special guest **Walfredo Reyes Jr.** on drums.
