---
title: "Vocalist Emily Jordan w/ Dan Karlsberg & Michael Sharfe"
date: '2021-12-10'
time: '7:30pm - 10:30pm'
image: '/images/events/emily-jordan.png'
thumb: '/images/events/thumbs/emily-jordan-thumb.jpg'
---
Vocalist **Emily Jordan**, with  
**Dan Karlsberg** on piano, and  
**Michael Sharfe** on bass
