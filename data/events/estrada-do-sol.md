---
title: 'Estrada do Sol'
date: '2021-10-29'
time: '7:30pm - 10:30pm'
image: '/images/events/estrada-do-sol.png'
thumb: '/images/events/thumbs/estrada-do-sol-thumb.png'
---
## The Music of Brazil  
### including Bossa Novas, Bahia, and Samba  
featuring vocalist **Andrea Cefalo**, pianist **Brian Cashwell**, & guitarist **Jorge Zillow**
