---
title: 'Dan Karlsberg (piano), Michael Sharfe (bass), & Michael Hyman (drums)'
date: '2021-04-24'
time: '7pm - 10pm'
thumb: '/images/events/thumbs/michael-hyman-thumb.png'
---
**Michael Hyman** is a Louisville native whose professional career has spanned over 50 years. Although based in jazz, his accomplishments as a drummer cross many genres.  

Currently, Michael teaches jazz studies at the University of Louisville and performs locally.

Don't miss this special guest!
