---
title: 'The Lou Lausche Trio, with special guest Lynne Scott'
date: '2021-10-23'
time: '7:30pm - 10:30pm'
image: '/images/events/lausche-scott.jpg'
thumb: '/images/events/thumbs/lynne-scott.png'
---
Featuring  
**Lou Lausche** on bass,  
**Lynne Scott** on vocals,  
**Bob Roetker** on guitar, &  
**Mike Meloy** on drums
