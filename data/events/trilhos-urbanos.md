---
title: 'Trilhos Urbanos'
date: '2021-10-30'
time: '7:30pm - 10:30pm'
image: '/images/events/trilhos-urbanos.png'
thumb: '/images/events/thumbs/trilhos-urbanos-thumb.png'
---
A trip through early 20th century Brazilian songs and classics!  

Featuring **Baba Charles Miller** on percussion,  
**Paula Gandara** on vocals, and  
**Joao Rocha** on piano
