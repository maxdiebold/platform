---
title: 'John Zappa (trumpet), Jim Connerley (piano), & Michael Sharfe (bass)'
date: '2021-03-19'
time: '7pm - 10pm'
thumb: '/images/events/thumbs/john-zappa.png'
image: '/images/events/john-jim-mike.png'
---

