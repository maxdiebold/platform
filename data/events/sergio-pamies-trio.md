---
title: "Sergio Pamies Piano Trio"
date: '2021-12-18'
time: '7:30pm - 10:30pm'
image: '/images/events/sergio-pamies.png'
thumb: '/images/events/thumbs/sergio-pamies-thumb.png'
---
Featuring **Sergio Pamies** on piano,  
**Michael Sharfe** on bass, &  
**Jeff Mellott** on drums
