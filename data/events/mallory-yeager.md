---
title: 'Pamela Mallory with the Wayne Yeager Trio'
date: '2021-11-13'
time: '7:30pm - 10:30pm'
image: '/images/events/mallory-yeager.png'
thumb: '/images/events/thumbs/pam-mallory-thumb.png'
---
**Pam Mallory** on vocals, with  
**Wayne Yeager** on piano,  
**Michael Sharfe** on bass, and  
**Melvin Broach** on drums
