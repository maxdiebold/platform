---
title: "Phil DeGreg with Michael Sharfe & Kiko Sebrian"
date: '2021-12-11'
time: '7:30pm - 10:30pm'
image: '/images/events/phil-degreg.png'
thumb: '/images/events/thumbs/phil-degreg-thumb.png'
---
**Phil DeGreg** on piano, with  
**Michael Sharfe** on bass, and  
**Kiko Sebrian** on drums
