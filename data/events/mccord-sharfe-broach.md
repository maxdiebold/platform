---
title: 'The Tim McCord Trio'
date: '2021-09-25'
time: '7:30pm - 10:30pm'
image: '/images/events/tim-mccord-2.png'
thumb: '/images/events/thumbs/tim-mccord-thumb.png'
---
Featuring sax player  
**Tim McCord** with  
**Michael Sharfe** on bass, &  
**Melvin Broach** on drums

