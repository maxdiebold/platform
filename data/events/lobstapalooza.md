---
title: 'Lobstapalooza IX'
date: '2021-06-04'
endDate: '2021-07-03'
time: ''
thumb: '/images/events/thumbs/lobster-thumb.png'
---
Fresh steamed **lobster** in a variety of preparations! Including:  
Bourbon Roasted Whole Maine Lobster, Lobster Curry, Lobstabellas, Lobster Bisque, Champagne Pesto Lobster Tails, Lobster BLT, Lobster Poppers, Lobstocado Salad, and more!  

New this year: **$22 Whole Maine Lobster Dinner**, available every Thursday during the festival, *all day long*.
