---
title: 'Shrimp Jamboree'
date: '2021-11-16'
image: '/images/posts/shrimp-boil.jpg'
---
The **Shrimp Jamboree** menu (right) kicks off this *Friday, November 19th*!  

And mark your calendars for the **Low Country Shrimp Boil** (left) on *Friday, December 3rd*!
