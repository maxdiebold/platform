---
title: 'Murder on the Menu'
date: '2021-05-25'
image: '/images/posts/mom.jpg'
---
Corpses in the alley, corpses in the house, and corpses in the park!  
Join us for historically inspired menus and beverage pairings served with a side of real life
murder from Cincinnati's past, including Harry Baldwin, Anna Marie Hahn, and George Remus.  

- **Thursday, June 17th:** Harry Baldwin Murder  
- **Saturday Brunch, June 19th:** Harry Baldwin Murder  
- **Thursday, July 15th:** Anna Marie Hahn Story  
- **Thursday, August 19th:** Harry Baldwin Murder  
- **Thursday, September 16th:** Anna Marie Hahn Story  
- **Thursday, October 21st:** Harry Baldwin Murder  

These events – entertainment, dinner and beer (Baldwin) or
wine (Hahn) pairings – are **all-inclusive experiences for 60.00**.  

**Thursday, November 18th**: *The Imogene & George Remus Story* - **70.00**  
A special Prohibition Era menu & whiskey event, featuring George Remus Whiskey.  

Purchase tickets in advance at [Cincy Murder Dinners](https://cincymurderdinners.com)
