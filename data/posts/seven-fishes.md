---
title: 'Feast of the Seven Fishes'
date: '2021-12-09'
image: '/images/posts/seven-fishes.png'
---
A traditional 7-course meal featuring all of the best seafood!  

Here's last year's menu to tempt you:
- **Oysters on the 1/2 Shell**
- **Chilled Shrimp Louise**
- **Seafood Bisque**
- **Calamari Caesar Salad**
- **Sole Almondine**
- **Linguini, Mussels, & Clams in a Pesto Cream**
- **Crab-stuffed Salmon with Hollandaise**
- **Canoli**  
  
Reservations are required and are available from **4pm - 9pm** on the **23rd** and the **24th**.  
Price is **55** per person, or **80** with wine pairings.
