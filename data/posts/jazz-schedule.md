---
title: 'Upcoming Jazz'
date: '2021-11-01'
image: '/images/posts/jazz-schedule.png'
---
**Lots of jazz on the way!**  

We've provided a schedule here as a quick reference, but be sure to check out the [Events](https://washingtonplatform.com/events) page for more details!
